# sisop-praktikum-fp-2023-MHFD-IT06

- Fathika Afrine Azaruddin - 5027211016
- Athallah Narda Wiyoga - 5027211041
- Gilbert Immanuel Hasiholan - 5027211056

## Client

1. Direktif Preprocessor:
   ```c
   #include <stdio.h>
   #include <stdlib.h>
   #include <string.h>
   #include <unistd.h>
   #include <sys/socket.h>
   #include <arpa/inet.h>
   ```
   Bagian ini merupakan direktif preprocessor yang meng-include library yang diperlukan untuk program ini, seperti standar input/output (`stdio.h`), alokasi memori (`stdlib.h`), operasi string (`string.h`), system call (`unistd.h`), dan operasi socket dan jaringan (`sys/socket.h` dan `arpa/inet.h`).

2. Konstanta:
   ```c
   #define PORT 8080
   #define BUFFER_SIZE 1024
   ```
   Konstanta `PORT` menentukan nomor port yang digunakan untuk koneksi socket, sementara `BUFFER_SIZE` menentukan ukuran buffer untuk membaca dan mengirim data.

3. Fungsi `authenticateUser()`:
   ```c
   int authenticateUser(const char *username, const char *password) {
       // ...
   }
   ```
   Fungsi ini bertujuan untuk memeriksa dan mengautentikasi pengguna berdasarkan informasi yang ada di file "users.csv". Fungsi ini membuka file "users.csv" dan membaca setiap baris di dalamnya. Setiap baris kemudian dipecah-pecah menjadi token menggunakan fungsi `strtok()` dengan pemisah koma. Username dan password yang ada di file "users.csv" dibandingkan dengan username dan password yang diberikan sebagai argumen fungsi. Jika ada kecocokan, fungsi ini mengembalikan nilai 1 yang menunjukkan autentikasi berhasil, jika tidak ada kecocokan, fungsi ini mengembalikan nilai 0.

4. Fungsi `main()`:
   ```c
   int main(int argc, char *argv[]) {
       // ...
   }
   ```
   Fungsi `main()` merupakan fungsi utama program ini. Fungsi ini memiliki beberapa langkah untuk menjalankan program client-server.

   a. Inisialisasi Variabel:
      ```c
      int sock = 0;
      struct sockaddr_in serv_addr;
      char buffer[BUFFER_SIZE] = {0};
      const char *username;
      const char *password;
      ```
      Langkah pertama adalah mendeklarasikan dan menginisialisasi variabel yang diperlukan. Variabel `sock` digunakan untuk menyimpan file descriptor socket, `serv_addr` adalah struktur untuk menyimpan alamat server, dan `buffer` adalah buffer yang digunakan untuk membaca dan mengirim data melalui socket. Variabel `username` dan `password` akan digunakan untuk menyimpan informasi autentikasi pengguna.

   b. Pengecekan Argumen Baris Perintah:
      ```c
      if (geteuid() == 0) {
          username = "root";
          password = "root";
      } else {
          // Check command line arguments
          if (argc != 5 || strcmp(argv[1], "-u") != 0 || strcmp(argv[3], "-p") != 0) {
              printf("Invalid arguments.\n");
              printf("Usage: %s -u [username] -p [password]\n", argv[0]);
              exit(EXIT_FAILURE);
          }
          username = argv[

2];
          password = argv[4];
      }
      ```
      Program melakukan pengecekan apakah dijalankan sebagai root menggunakan `geteuid()`. Jika dijalankan sebagai root, maka username dan password akan diatur sebagai "root". Jika bukan root, program akan memeriksa argumen baris perintah untuk mendapatkan username dan password. Program mengecek apakah jumlah argumen sesuai dengan yang diharapkan (`argc != 5`), dan apakah argumen "-u" dan "-p" ada pada posisi yang tepat (`strcmp(argv[1], "-u") != 0 || strcmp(argv[3], "-p") != 0`). Jika terjadi kesalahan dalam argumen baris perintah, program akan menampilkan pesan kesalahan dan keluar.

   c. Parsing Argumen Baris Perintah:
      ```c
      for (int i = 1; i < argc; i++) {
          if (strcmp(argv[i], "-u") == 0 && i + 1 < argc) {
              username = argv[i + 1];
          } else if (strcmp(argv[i], "-p") == 0 && i + 1 < argc) {
              password = argv[i + 1];
          }
      }
      ```
      Jika argumen baris perintah valid, program akan melakukan parsing argumen tersebut menggunakan loop. Program mencari argumen "-u" dan "-p" dan mengambil nilai username dan password yang sesuai.

   d. Autentikasi Pengguna:
      ```c
      if (username == NULL || password == NULL) {
          printf("Invalid command line arguments\n");
          exit(EXIT_FAILURE);
      }
      if (!authenticateUser(username, password)) {
          printf("Authentication failed\n");
          exit(EXIT_FAILURE);
      }
      ```
      Program melakukan pemeriksaan terakhir terhadap username dan password yang diperoleh. Jika ada kesalahan dalam parsing argumen atau jika autentikasi gagal melalui fungsi `authenticateUser()`, program akan menampilkan pesan kesalahan dan keluar.

   e. Membuat Socket:
      ```c
      if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
          perror("socket failed");
          exit(EXIT_FAILURE);
      }
      ```
      Program membuat socket menggunakan `socket()` dan menyimpan file descriptor socket dalam variabel `sock`. Jika pembuatan socket gagal, program akan menampilkan pesan kesalahan dan keluar.

   f. Menginisialisasi Alamat Server:
      ```c
      serv_addr.sin_family = AF_INET;
      serv_addr.sin_port = htons(PORT);
      if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
          perror("inet_pton failed");
          exit(EXIT_FAILURE);
      }
      ```
      Program menginisialisasi alamat server dalam variabel `serv_addr`. `sin_family` diset sebagai `AF_INET` untuk menggunakan IPv4, `sin_port` diset dengan nomor port yang telah ditentukan menggunakan `htons(PORT)`, dan `sin_addr` diset dengan alamat IP yang ditetapkan sebagai "127.0.0.1" menggunakan `inet_pton()`.

   g. Koneksi ke Server:
      ```c
      if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
          perror("connect failed");
          exit(EXIT_FAILURE);
      }
      ```
      Program melakukan koneksi ke server menggunakan `connect

()` dengan menyediakan file descriptor socket, alamat server, dan ukuran struktur alamat.

   h. Mengirim Autentikasi ke Server:
      ```c
      snprintf(buffer, BUFFER_SIZE, "%s,%s", username, password);
      send(sock, buffer, strlen(buffer), 0);
      ```
      Program mengirimkan username dan password ke server dengan menggabungkan keduanya dalam format string menggunakan `snprintf()`, lalu mengirimkan string tersebut melalui socket menggunakan `send()`.

   i. Loop Utama:
      ```c
      while (1) {
          printf("Enter a string (or press Ctrl+C to exit): ");
          fgets(buffer, BUFFER_SIZE, stdin);
          send(sock, buffer, strlen(buffer), 0);
          // ...
      }
      ```
      Program memasuki loop utama yang memungkinkan pengguna untuk memasukkan pesan yang akan dikirim ke server. Program menampilkan prompt untuk memasukkan string, kemudian membaca string dari pengguna menggunakan `fgets()`. String tersebut kemudian dikirim melalui socket menggunakan `send()`.

   j. Menerima Respon dari Server:
      ```c
      read(sock, buffer, BUFFER_SIZE);
      printf("Server: %s\n", buffer);
      ```
      Program membaca respon dari server menggunakan `read()`, dan menampilkan respon tersebut di layar.

   k. Mengulang Loop Utama:
      ```c
      memset(buffer, 0, BUFFER_SIZE);
      ```
      Setelah menerima respon dari server, buffer direset menjadi nol menggunakan `memset()`. Hal ini untuk memastikan bahwa buffer kosong saat membaca input berikutnya.

   l. Penutup:
      ```c
      return 0;
      ```
      Program keluar dari fungsi `main()` dan mengembalikan nilai 0, menandakan bahwa program selesai dijalankan tanpa ada kesalahan.

## Database

Tentu, berikut adalah penjelasan baris per baris dari kode yang diberikan:

1. Mendefinisikan Header:
   ```c
   #include <stdio.h>
   #include <stdlib.h>
   #include <string.h>
   #include <unistd.h>
   #include <sys/socket.h>
   #include <arpa/inet.h>
   ```

   Ini adalah bagian di mana kita mendefinisikan header yang diperlukan untuk menggunakan fungsi-fungsi standar dan operasi soket.

2. Mendefinisikan Konstanta:
   ```c
   #define PORT 8080
   #define BUFFER_SIZE 1024
   #define CSV_FILE "users.csv"
   ```

   Di sini, kita mendefinisikan beberapa konstanta:
   - `PORT`: Nomor port yang akan digunakan oleh server.
   - `BUFFER_SIZE`: Ukuran buffer yang digunakan untuk mengirim dan menerima data.
   - `CSV_FILE`: Nama file CSV yang akan digunakan untuk menyimpan informasi pengguna.

3. Fungsi `createUser`:
   ```c
   void createUser(const char *username, const char *password) {
       FILE *file = fopen(CSV_FILE, "a");
       if (file == NULL) {
           perror("Failed to open users.csv");
           exit(EXIT_FAILURE);
       }
   
       fprintf(file, "%s,%s,false\n", username, password);
       fclose(file);
   }
   ```

   Fungsi ini bertujuan untuk membuat entri pengguna baru dalam file CSV `users.csv`. Fungsi ini membuka file dengan mode "a" (menambahkan pada akhir file), menulis entri pengguna baru, dan menutup file.

4. Fungsi `userExists`:
   ```c
   int userExists(const char *username) {
       FILE *file = fopen(CSV_FILE, "r");
       if (file == NULL) {
           perror("Failed to open users.csv");
           exit(EXIT_FAILURE);
       }
   
       char line[BUFFER_SIZE];
       while (fgets(line, BUFFER_SIZE, file)) {
           char *user = strtok(line, ",");
           if (user != NULL && strcmp(user, username) == 0) {
               fclose(file);
               return 1;
           }
       }
   
       fclose(file);
       return 0;
   }
   ```

   Fungsi ini bertujuan untuk memeriksa apakah pengguna dengan nama tertentu sudah ada dalam file CSV `users.csv`. Fungsi ini membuka file dalam mode "r" (membaca file), membaca setiap baris dari file CSV, dan memeriksa apakah pengguna ditemukan berdasarkan username. Fungsi ini mengembalikan 1 jika pengguna ditemukan dan 0 jika tidak.

5. Fungsi `handleClient`:
   ```c
   void handleClient(int client_socket, const char* u_username, const char* u_password) {
       char buffer[BUFFER_SIZE];
       int valread;
   
       valread = read(client_socket, buffer, BUFFER_SIZE);
       printf("Client: %s\n", buffer);
   
       // Check if the request is to create a new user
       if (strncmp(buffer, "CREATE USER", 11) == 0) {
           // Check if the user is root
           if (u_username != NULL && u_password != NULL && strcmp(u_username, "root") == 0 && strcmp(u_password, "root") == 0) {
               char *username = strtok(buffer +

 12, " ");
               char *identified = strtok(NULL, " ");
               char *by = strtok(NULL, " ");
               char *password = strtok(NULL, ";");
   
               if (username != NULL && identified != NULL && by != NULL && password != NULL && strcmp(identified, "IDENTIFIED") == 0 && strcmp(by, "BY") == 0) {
                   // Check if the user already exists
                   if (userExists(username)) {
                       send(client_socket, "User already exists\n", 20, 0);
                       printf("Response sent\n");
                   } else {
                       // Create a new user
                       createUser(username, password);
                       send(client_socket, "User created\n", 13, 0);
                       printf("Response sent\n");
                   }
               } else {
                   send(client_socket, "Invalid command\n", 16, 0);
                   printf("Response sent\n");
               }
           } else {
               send(client_socket, "Insufficient privileges\n", 24, 0);
               printf("Response sent\n");
           }
       } else {
           // Echo the received message back to the client
           send(client_socket, buffer, strlen(buffer), 0);
           printf("Response sent\n");
       }
       memset(buffer, 0, BUFFER_SIZE);
   }
   ```

   Fungsi ini bertanggung jawab untuk menangani permintaan dari klien yang terhubung. Fungsi ini membaca pesan yang diterima dari klien, memeriksa jenis permintaan, dan memberikan respons sesuai dengan permintaan tersebut. Jika permintaan adalah "CREATE USER", fungsi akan memeriksa apakah pengguna yang meminta operasi ini adalah pengguna "root" dan kemudian menciptakan pengguna baru jika syarat terpenuhi. Jika permintaan bukan "CREATE USER", fungsi akan mengirim kembali pesan yang sama ke klien (fungsi ini bertindak sebagai echo server).

6. Fungsi `main`:
   ```c
   int main() {
       int server_fd, new_socket;
       struct sockaddr_in address;
       int opt = 1;
       int addrlen = sizeof(address);
       char buffer[BUFFER_SIZE] = {0};
       char* u_username = NULL;
       char* u_password = NULL;
   
       // Create socket file descriptor
       if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
           perror("socket failed");
           exit(EXIT_FAILURE);
       }
   
       // Forcefully attach socket to the port 8080
       if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
           perror("setsockopt failed");
           exit(EXIT_FAILURE);
       }
       address.sin_family = AF_INET;
       address.sin_addr.s_addr = INADDR_ANY;
       address.sin_port = htons(PORT);
   
       // Bind the socket to localhost port 8080
       if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
           perror("bind failed");
           exit(EXIT_FAILURE);
       }
   
       // Listen for incoming connections
       if (listen(server_fd, 3) < 0) {
           perror("listen failed");
           exit(EXIT_FAILURE);
       }
   
       // Accept incoming connections and handle them
       if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
               perror("accept failed");
               exit(EXIT_FAILURE);
       }
   
       // Read string from client
       read(new_socket, buffer, BUFFER

_SIZE);
       printf("Client: %s\n", buffer);
   
       u_username = strtok(buffer, ",");
       u_password = strtok(NULL, ",");
   
       while (1) {
           handleClient(new_socket, u_username, u_password);
       }
   
       return 0;
   }
   ```

   Fungsi `main` adalah entry point dari program. Di dalam fungsi ini, socket server dibuat, diikat ke alamat dan port yang ditentukan, kemudian diterima koneksi dari klien. Setelah koneksi terbentuk, pesan dari klien dibaca untuk mendapatkan informasi username dan password. Selanjutnya, fungsi `handleClient` dipanggil secara terus-menerus untuk menangani permintaan dari klien. Program berjalan dalam loop tak terhingga (`while (1)`) untuk menerima dan menangani permintaan dari klien secara berkesinambungan.

## Output
![check](https://cdn.discordapp.com/attachments/1088521761596919869/1122342313037934612/image.png)
![check](https://cdn.discordapp.com/attachments/1088521761596919869/1122342573890097252/image.png)