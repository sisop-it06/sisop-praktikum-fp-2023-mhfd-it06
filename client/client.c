#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#define PORT 8080
#define BUFFER_SIZE 1024

// Function to authenticate user and check access status
int authenticateUser(const char *username, const char *password) {
    FILE *file = fopen("users.csv", "r");
    if (file == NULL) {
        perror("Cannot open users.csv");
        exit(EXIT_FAILURE);
    }

    char line[BUFFER_SIZE];
    char *token;

    while (fgets(line, BUFFER_SIZE, file)) {
        // Tokenize each line based on comma separator
        token = strtok(line, ",");
        if (token == NULL) {
            continue;
        }
        char *csvUsername = token;

        token = strtok(NULL, ",");
        if (token == NULL) {
            continue;
        }
        char *csvPassword = token;

        // Compare username and password
        if (strcmp(username, csvUsername) == 0 && strcmp(password, csvPassword) == 0) {
            fclose(file);
            return 1;
        }
    }

    fclose(file);
    return 0;
}

int main(int argc, char *argv[]) {
    int sock = 0;
    struct sockaddr_in serv_addr;
    char buffer[BUFFER_SIZE] = {0};

    const char *username;
    const char *password;

    // Check if program is executed with sudo
    if (geteuid() == 0) {
        username = "root";
        password = "root";
    } else {
        // Check command line arguments
        if (argc != 5 || strcmp(argv[1], "-u") != 0 || strcmp(argv[3], "-p") != 0) {
            printf("Invalid arguments.\n");
            printf("Usage: %s -u [username] -p [password]\n", argv[0]);
            exit(EXIT_FAILURE);
        }
        username = argv[2];
        password = argv[4];
    }

    // Parse command line arguments
    for (int i = 1; i < argc; i++) {
        if (strcmp(argv[i], "-u") == 0 && i + 1 < argc) {
            username = argv[i + 1];
        } else if (strcmp(argv[i], "-p") == 0 && i + 1 < argc) {
            password = argv[i + 1];
        }
    }

    if (username == NULL || password == NULL) {
        printf("Invalid command line arguments\n");
        exit(EXIT_FAILURE);
    }

    // Authenticate user
    if (!authenticateUser(username, password)) {
        printf("Authentication failed\n");
        exit(EXIT_FAILURE);
    }

    // Create socket file descriptor
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    // Convert IPv4 and IPv6 addresses from text to binary form
    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
        perror("inet_pton failed");
        exit(EXIT_FAILURE);
    }

    // Connect to the server
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        perror("connect failed");
        exit(EXIT_FAILURE);
    }
    
    // Send username and password to the server
    snprintf(buffer, BUFFER_SIZE, "%s,%s", username, password);
    send(sock, buffer, strlen(buffer), 0);

    // Reset buffer
    memset(buffer, 0, BUFFER_SIZE);
   

    while (1) {
        printf("Enter a string (or press Ctrl+C to exit): ");
        fgets(buffer, BUFFER_SIZE, stdin);

        // Send string to server
        send(sock, buffer, strlen(buffer), 0);
        printf("Message sent\n");

        // Receive response from server
        read(sock, buffer, BUFFER_SIZE);
        printf("Server: %s\n", buffer);
        
        // Reset buffer
        memset(buffer, 0, BUFFER_SIZE);
    }

    return 0;
}
