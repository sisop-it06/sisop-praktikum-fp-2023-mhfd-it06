#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#define PORT 8080
#define BUFFER_SIZE 1024
#define CSV_FILE "users.csv"

// Function to create a new user in the CSV file
void createUser(const char *username, const char *password) {
    FILE *file = fopen(CSV_FILE, "a");
    if (file == NULL) {
        perror("Failed to open users.csv");
        exit(EXIT_FAILURE);
    }
    
    

    fprintf(file, "%s,%s,false\n", username, password);
    fclose(file);
}

// Function to check if a user exists in the CSV file
int userExists(const char *username) {
    FILE *file = fopen(CSV_FILE, "r");
    if (file == NULL) {
        perror("Failed to open users.csv");
        exit(EXIT_FAILURE);
    }

    char line[BUFFER_SIZE];
    while (fgets(line, BUFFER_SIZE, file)) {
        char *user = strtok(line, ",");
        if (user != NULL && strcmp(user, username) == 0) {
            fclose(file);
            return 1;
        }
    }

    fclose(file);
    return 0;
}

// Function to handle client requests
void handleClient(int client_socket, const char* u_username, const char* u_password) {
    char buffer[BUFFER_SIZE];
    int valread;

    valread = read(client_socket, buffer, BUFFER_SIZE);
    printf("Client: %s\n", buffer);

    // Check if the request is to create a new user
    if (strncmp(buffer, "CREATE USER", 11) == 0) {
        // Check if the user is root
        if (u_username != NULL && u_password != NULL && strcmp(u_username, "root") == 0 && strcmp(u_password, "root") == 0) {
            char *username = strtok(buffer + 12, " ");
            char *identified = strtok(NULL, " ");
        char *by = strtok(NULL, " ");
        char *password = strtok(NULL, ";");

            if (username != NULL && identified != NULL && by != NULL && password != NULL && strcmp(identified, "IDENTIFIED") == 0 && strcmp(by, "BY") == 0) {
            // Check if the user already exists
                if (userExists(username)) {
                    send(client_socket, "User already exists\n", 20, 0);
                    printf("Response sent\n");
                } else {
                    // Create a new user
                    createUser(username, password);
                    send(client_socket, "User created\n", 13, 0);
                    printf("Response sent\n");
                }
            } else {
                send(client_socket, "Invalid command\n", 16, 0);
                printf("Response sent\n");
            }
        } else {
            send(client_socket, "Insufficient privileges\n", 24, 0);
            printf("Response sent\n");
        }
    } else {
        // Echo the received message back to the client
        send(client_socket, buffer, strlen(buffer), 0);
        printf("Response sent\n");
    }
    memset(buffer, 0, BUFFER_SIZE);
}

int main() {
    int server_fd, new_socket;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[BUFFER_SIZE] = {0};
    char* u_username = NULL;
    char* u_password = NULL;

    // Create socket file descriptor
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    // Forcefully attach socket to the port 8080
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt failed");
        exit(EXIT_FAILURE);
    }
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    // Bind the socket to localhost port 8080
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    // Listen for incoming connections
    if (listen(server_fd, 3) < 0) {
        perror("listen failed");
        exit(EXIT_FAILURE);
    }

    // Accept incoming connections and handle them
    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
            perror("accept failed");
            exit(EXIT_FAILURE);
    }
    
    // Read string from client
        read(new_socket, buffer, BUFFER_SIZE);
        printf("Client: %s\n", buffer);

        u_username = strtok(buffer, ",");
        u_password = strtok(NULL, ",");
            
    
    while (1) {
      
        handleClient(new_socket, u_username, u_password);
        
    }

    return 0;
}
